package firstjavaprogram;

import java.util.Scanner;

public class MortgageCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//mortgage=principal*(monthly interest*Math.pow(1+
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Loan Amount : ");
		int loanAmount=sc.nextInt();
		
		System.out.println("Enter Annual Interest Rate : ");
		float annualInterest=sc.nextFloat();
		
		System.out.println("enter Period : ");
		byte period=sc.nextByte();
		
		int percent=100;
		int monthsInYear=12;
		int numberOfPayments=12*period;
		float monthlyInterest=annualInterest/percent/monthsInYear;
		
		double emi=loanAmount*(monthlyInterest*Math.pow(1+monthlyInterest, numberOfPayments))/(Math.pow(1+monthlyInterest, numberOfPayments)-1);
		 
		System.out.println("EMI amount for inserted values is: "+emi);

	}

}
